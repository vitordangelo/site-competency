<?php
$name = $_POST[name];
$email = $_POST[email];
$subject = $_POST[subject];
$message = $_POST[message];

//Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
require("phpmailer/class.phpmailer.php");

//Inicia a classe PHPMailer
$mail = new PHPMailer();

//Define os dados do servidor e tipo de conexão
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->Host = "smtp.competency.com.br"; // Endereço do servidor SMTP
$mail->SMTPAuth = true; // Autenticação
$mail->Username = 'contato@competency.com.br'; // Usuário do servidor SMTP
$mail->Password = 'sua senha'; // Senha da caixa postal utilizada

//Define o remetente
$mail->From = $email; 
$mail->FromName = $name;

//Define os destinatário(s)
$mail->AddAddress('contato@competency.com.br', 'Competency');
// $mail->AddAddress('e-mail@destino2.com.br');
// $mail->AddCC('copia@dominio.com.br', 'Copia'); 
// $mail->AddBCC('CopiaOculta@dominio.com.br', 'Copia Oculta');

//Define os dados técnicos da Mensagem
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

//Texto e Assunto
$mail->Subject  = $subject; // Assunto da mensagem
$mail->Body = $message;
$mail->AltBody = $message;

//Anexos (opcional)
// $mail->AddAttachment("e:\home\login\web\documento.pdf", "novo_nome.pdf");

//Envio da Mensagem
$enviado = $mail->Send();

//Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();

//Exibe uma mensagem de resultado
if ($enviado) {
    <script>
        alert("Email enviado!");
    </script>
} else {
echo "Não foi possível enviar o e-mail.
 
";
echo "Informações do erro: 
" . $mail->ErrorInfo;
}
$>