var gulp = require('gulp'),
    broswerSync = require('browser-sync'),
    // favicons = require("gulp-favicons"),
    gutil = require("gulp-util");


gulp.task('server', function() {
    broswerSync.init({
        server: {
            baseDir: 'src'
        }
    });

    gulp.watch('src/**/*').on('change', broswerSync.reload);
});

gulp.task("favicon", function () {
    return gulp.src("./src/images/logo.png").pipe(favicons({
        appName: "My App",
        appDescription: "This is my application",
        developerName: "Hayden Bleasel",
        developerURL: "http://haydenbleasel.com/",
        background: "#020307",
        path: "favicons/",
        url: "http://haydenbleasel.com/",
        display: "standalone",
        orientation: "portrait",
        start_url: "/?homescreen=1",
        version: 1.0,
        logging: false,
        online: false,
        html: "index.html",
        pipeHTML: true,
        replace: true
    }))
    .on("error", gutil.log)
    .pipe(gulp.dest("./images/favicons"));
});
